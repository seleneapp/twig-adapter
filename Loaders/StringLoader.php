<?php

/**
 * This File is part of the Selene\Adapters\Twig package
 *
 * (c) Thomas Appel <mail@thomas-appel.com>
 *
 * For full copyright and license information, please refer to the LICENSE file
 * that was distributed with this package.
 */


namespace Selene\Adapters\Twig\Loaders;

use \Selene\Components\View\Template\ResolverInterface;
use \Selene\Components\View\Template\LoaderInterface as TemplateLoaderInterface;

/**
 * @class StringLoader extends \Twig_Loader_String implements TemplateLoaderInterface
 * @see TemplateLoaderInterface
 * @see \Twig_Loader_String
 *
 * @package Selene\Adapters\Twig\Loaders
 * @version $Id$
 * @author Thomas Appel <mail@thomas-appel.com>
 */
class StringLoader extends \Twig_Loader_String implements TemplateLoaderInterface
{
    /**
     * load
     *
     * @param mixed $name
     *
     * @return string
     */
    public function load($name)
    {
        return $name;
    }

    public function exists($name)
    {
        return true;
    }

    /**
     * isValid
     *
     * @param string $name
     * @param int $time
     *
     * @return boolean
     */
    public function isValid($name, $time)
    {
        return $this->isFresh($name, $time);
    }
}
